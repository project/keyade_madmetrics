
DESCRIPTION
===========

The Keyade Madmetrics module provides integration with the Keyade Madmetrics,
allowing you to measures media campaign effectiveness and providing campaign
optimization decision support.

The module provides the Keyade Madmetrics tags for the following events:

  - user registration
  - checkout completion

Additionally, it supports tag for the newsletter subscription event, this one
however needs to be custom-called from your code based on your specific
implementation (using keyade_madmetrics_tag_newsletter_signup() function).

Finally, it provides relevant Google Tag Manager Data Layer variables
for each tag.



INSTALLATION
============

1. Enable the Keyade Madmetrics module.

2. Configure the Keyade Madmetrics module by providing your Client ID
   (as given to you by Keyade Support).



MORE INFORMATION
================

* Keyade Madmetrics
  http://www.keyade.com/en/#madmetrics

* Google Tag Manager
  https://www.google.fr/analytics/tag-manager/

* Google Tag Manager Developer Guide (Data Layer)
  https://developers.google.com/tag-manager/devguide
